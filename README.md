# Bildverarbeitung Übungen

In diesem Repository befinden sich Übungen zur Lehrveranstalung *Bildverarbeitung*.
Die Übungen sind als [Jupyter Notebooks](https://jupyter.org/) angelegt und können damit einfach im Browser bearbeitet werden.


## Online
Die notebooks können online über [Binder](https://mybinder.org/) bearbeitet werden.
Dazu bitte auf folgenden Button klicken:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.ub.uni-bielefeld.de%2Fbildverarbeitung%2Fexcercises.git/HEAD?urlpath=tree/)

**Hinweis**: Der Service den Binder liefert ist kostenlos und dementsprechend sind die verfügbaren Resourcen begrenzt.
Es kann also lange dauern bis die Notebooks gestartet sind und Sessions haben ein geringes Timeout bis sie beendet werden.
Also lohnt es sich für alle die sich etwas damit auskennen die Offline-Variante zu nutzen.

## Offline
1. Clone this repository: `git clone https://gitlab.ub.uni-bielefeld.de/bildverarbeitung/excercises.git`
2. Enter repository folder: `cd excercises`
3. Install requirements: `pip install -r requirements.txt`
4. Launch Jupyter Notebook: `jupyter notebook`

## Offline - Docker
1. Clone this repository: `git clone https://gitlab.ub.uni-bielefeld.de/bildverarbeitung/excercises.git`
2. Enter repository folder: `cd excercises`
3. Install docker image: `docker build -t bv-excercises .`
4. Launch docker container: `docker run -it -p 8888:8888 bv-excercises jupyter notebook --ip=0.0.0.0 --port=8888`

